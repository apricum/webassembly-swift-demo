import { fetchModule, instantiateModule, WebAssemblyModule } from "./module"

// State

let globalModule: WebAssemblyModule | undefined

// Init

async function assertedModule(): Promise<WebAssemblyModule> {
	if (globalModule) {
		return globalModule
	}

	const moduleData = await fetchModule("/color-theme-assembly.wasm")
	globalModule = await instantiateModule(moduleData)

	return globalModule
}

async function runDemo(modeData: string, inputData: string): Promise<string | undefined> {
	try {
		const module = await assertedModule()
		const argumentAddress = await module.initializeStringValue(inputData)
		const results = await module.callStreamingFunctionWithArgument(modeData, argumentAddress)
		await module.deinitializeStringValue(argumentAddress)

		return results
	} catch (error) {
		console.error(error)
		return "<Error>"
	}
}

function safeDecodedJSONData(data: string): object | undefined {
	try {
		return JSON.parse(data)
	} catch {
		return undefined
	}
}

function formattedDataForOutput(data: string): string {
	const jsonData = safeDecodedJSONData(data)
	if (!jsonData) {
		return data
	}

	return JSON.stringify(jsonData, null, 4)
}

window.addEventListener("load", () => {
	assertedModule()

	const [mode, input, output, action, debug] = [
		document.querySelector("#mode") as HTMLSelectElement,
		document.querySelector("#input") as HTMLInputElement,
		document.querySelector("#output") as HTMLElement,
		document.querySelector("#action") as HTMLButtonElement,
		document.querySelector("#debug") as HTMLElement
	]

	// Run initial call after load for convenience.
	setTimeout(() => {
		input.value = ""
		action.click()
	}, 25)

	action.addEventListener("click", async () => {
		const startTime = performance.now()

		const modeData = mode.value ?? ""
		const inputData = input?.value ?? ""
		const responseData = (await runDemo(modeData, inputData)) ?? ""

		const endTime = performance.now()
		const executionTime = endTime - startTime
		const executionTimePerUnit = executionTime / 1

		output.innerHTML = formattedDataForOutput(responseData)
		debug.innerHTML = `Execution time: ${executionTime.toFixed(2)}ms (${executionTimePerUnit.toFixed(2)}ms fractional).`
	})

	input.addEventListener("keydown", event => {
		if (event.code !== "Enter") {
			return true
		}

		action.click()
		return false
	})
})
